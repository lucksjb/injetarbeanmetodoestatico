package br.com.lckconsultoria;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import br.com.lckconsultoria.models.Funcionario;
import br.com.lckconsultoria.services.FuncionarioService;

/***
 * SPRING
 * Injetar componente e utilizar no metodo statico 
 * https://www.javaguides.net/2019/10/how-to-get-application-context-in-spring-boot.html
****/

@SpringBootApplication
public class InjetarBeanMetodoEstatico implements CommandLineRunner {
	@Autowired
	private ApplicationContext applicationContext;

	public static void main(String[] args) {
		SpringApplication.run(LckPontoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		FuncionarioService funcionarioService = applicationContext.getBean(FuncionarioService.class);
		
		
		Optional<Funcionario> funcionario = funcionarioService.busca(2L);
		if (funcionario.isPresent()) {
			System.out.println(funcionario.get().getNome());
		} else {
			System.out.println("funcinario nao encontrado ");
		}

	}

}
